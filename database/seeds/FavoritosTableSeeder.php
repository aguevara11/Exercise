<?php

use Illuminate\Database\Seeder;

class FavoritosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('favoritos')->insert(
        [
          'codigousuario' => 6,
          'codigousuariofavorito' =>7,
        ]
      );
      DB::table('favoritos')->insert(
        [
          'codigousuario' => 6,
          'codigousuariofavorito' =>8,
        ]
      );
    }
}
