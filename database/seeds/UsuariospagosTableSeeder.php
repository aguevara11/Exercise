<?php

use Illuminate\Database\Seeder;

class UsuariospagosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('usuariospagos')->insert(
        [
          'codigousuario' => 6,
          'codigopago' =>1,
        ]
      );
      DB::table('usuariospagos')->insert(
        [
          'codigousuario' => 5,
          'codigopago' =>1,
        ]
      );
      DB::table('usuariospagos')->insert(
        [
          'codigousuario' => 7,
          'codigopago' =>1,
        ]
      );
    }
}
