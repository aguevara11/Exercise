<?php

use Illuminate\Database\Seeder;

class PagosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
       DB::table('pagos')->insert(
         [
           'importe' => 100,
           'fecha' => '2018-03-10',
         ]

       );
       DB::table('pagos')->insert(
         [
           'importe' => 200,
           'fecha' => '2018-03-10',
         ]

       );
       DB::table('pagos')->insert(
         [
           'importe' => 300,
           'fecha' => '2018-03-10',
         ]

       );
     }
}
