<?php

use Illuminate\Database\Seeder;

class UsuariosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('usuarios')->insert(
        [
          'usuario' => 'Bartolome Esteban Murillo',
          'clave' => md5('Bartolome Esteban Murillo'),
          'edad' => 22,
        ]

    );
    DB::table('usuarios')->insert(
      [
        'usuario' => 'Diego Velázquez',
        'clave' => md5('Diego Velázquez'),
        'edad' => 22,
      ]

    );
    DB::table('usuarios')->insert(
      [
        'usuario' => 'Pablo Picasso',
        'clave' => md5('Pablo Picasso'),
        'edad' => 22,
      ]

    );
    DB::table('usuarios')->insert(
      [
        'usuario' => 'Salvador Dalí',
        'clave' => md5('Salvador Dalí'),
        'edad' => 22,
      ]

    );
    DB::table('usuarios')->insert(
      [
        'usuario' => 'Joan Miró',
        'clave' => md5('Joan Miró'),
        'edad' => 22,
      ]

    );
    DB::table('usuarios')->insert(
      [
        'usuario' => 'El Greco',
        'clave' => md5('El Greco'),
        'edad' => 22,
      ]

    );
    DB::table('usuarios')->insert(
      [
        'usuario' => 'Antonio López',
        'clave' => md5('Antonio López'),
        'edad' => 22,
      ]

    );
    DB::table('usuarios')->insert(
      [
        'usuario' => 'Juan Gris',
        'clave' => md5('Juan Gris'),
        'edad' => 22,
      ]

    );
    }
}
