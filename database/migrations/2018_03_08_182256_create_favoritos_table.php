<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFavoritosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('favoritos', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('codigousuario')->unsigned();
          $table->integer('codigousuariofavorito')->unsigned();
          $table->timestamps();
          $table->foreign('codigousuario')->references('codigousuario')->on('usuarios');
          $table->foreign('codigousuariofavorito')->references('codigousuario')->on('usuarios');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
