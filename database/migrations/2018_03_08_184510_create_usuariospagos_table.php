<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariospagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('usuariospagos', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('codigousuario')->unsigned();
        $table->integer('codigopago')->unsigned();
        $table->timestamps();
        $table->foreign('codigousuario')->references('codigousuario')->on('usuarios');
        $table->foreign('codigopago')->references('codigopago')->on('pagos');
      });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
