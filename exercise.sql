-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 09-03-2018 a las 23:51:16
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 7.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `exercise`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `favoritos`
--

CREATE TABLE `favoritos` (
  `id` int(10) UNSIGNED NOT NULL,
  `codigousuario` int(10) UNSIGNED NOT NULL,
  `codigousuariofavorito` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `favoritos`
--

INSERT INTO `favoritos` (`id`, `codigousuario`, `codigousuariofavorito`, `created_at`, `updated_at`) VALUES
(2, 1, 5, '2018-03-10 01:14:56', '2018-03-10 01:22:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2018_03_08_181706_create_usuarios_table', 1),
('2018_03_08_182256_create_favoritos_table', 1),
('2018_03_08_183655_create_pagos_table', 1),
('2018_03_08_184510_create_usuariospagos_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

CREATE TABLE `pagos` (
  `codigopago` int(10) UNSIGNED NOT NULL,
  `importe` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `pagos`
--

INSERT INTO `pagos` (`codigopago`, `importe`, `fecha`, `created_at`, `updated_at`) VALUES
(2, '100', '2018-11-11', '2018-03-10 01:20:11', '2018-03-10 01:20:11'),
(4, '1100', '2018-11-11', '2018-03-10 01:24:15', '2018-03-10 01:24:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `codigousuario` int(10) UNSIGNED NOT NULL,
  `usuario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `clave` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `edad` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`codigousuario`, `usuario`, `clave`, `edad`, `created_at`, `updated_at`) VALUES
(1, 'Bartolome Esteban Murillo', '6df349c743b7265c079a438c47ada510', 22, NULL, NULL),
(2, 'Diego Velázquez', 'c5ca99e07715f9c88d33dd9a8c797490', 22, NULL, NULL),
(3, 'Pablo Picasso', '9716719fb465c4004381d597ac4a7107', 22, NULL, NULL),
(5, 'Joan Miró', '0bd9e46666eefe16e79d2a210e2322eb', 22, NULL, NULL),
(7, 'Antonio López', 'bba49c9e3db62c6d8cbe2bded486c7b3', 22, NULL, NULL),
(8, 'Juan Gris', '7019146ecb236a98fbb96b80f9ad0aec', 22, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuariospagos`
--

CREATE TABLE `usuariospagos` (
  `id` int(10) UNSIGNED NOT NULL,
  `codigousuario` int(10) UNSIGNED NOT NULL,
  `codigopago` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `usuariospagos`
--

INSERT INTO `usuariospagos` (`id`, `codigousuario`, `codigopago`, `created_at`, `updated_at`) VALUES
(1, 1, 2, '2018-03-10 01:20:11', '2018-03-10 01:20:11');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `favoritos`
--
ALTER TABLE `favoritos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `favoritos_codigousuario_foreign` (`codigousuario`),
  ADD KEY `favoritos_codigousuariofavorito_foreign` (`codigousuariofavorito`);

--
-- Indices de la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD PRIMARY KEY (`codigopago`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`codigousuario`);

--
-- Indices de la tabla `usuariospagos`
--
ALTER TABLE `usuariospagos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usuariospagos_codigopago_foreign` (`codigopago`),
  ADD KEY `usuariospagos_codigousuario_foreign` (`codigousuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `favoritos`
--
ALTER TABLE `favoritos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `pagos`
--
ALTER TABLE `pagos`
  MODIFY `codigopago` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `codigousuario` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `usuariospagos`
--
ALTER TABLE `usuariospagos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `favoritos`
--
ALTER TABLE `favoritos`
  ADD CONSTRAINT `favoritos_codigousuario_foreign` FOREIGN KEY (`codigousuario`) REFERENCES `usuarios` (`codigousuario`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `favoritos_codigousuariofavorito_foreign` FOREIGN KEY (`codigousuariofavorito`) REFERENCES `usuarios` (`codigousuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuariospagos`
--
ALTER TABLE `usuariospagos`
  ADD CONSTRAINT `usuariospagos_codigopago_foreign` FOREIGN KEY (`codigopago`) REFERENCES `pagos` (`codigopago`) ON DELETE CASCADE,
  ADD CONSTRAINT `usuariospagos_codigousuario_foreign` FOREIGN KEY (`codigousuario`) REFERENCES `usuarios` (`codigousuario`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
