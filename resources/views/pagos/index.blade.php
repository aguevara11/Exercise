@extends('layouts.app')

@section('content')
<a href="{{url('/pagos/create')}}" role="button" class="btn btn-primary">Crear Pago</a>
<br>
<table class="table">
  <thead>
    <tr>
      <th scope="col">Codigo Pago</th>
      <th scope="col">Usuario</th>
      <th scope="col">Fecha</th>
      <th scope="col">Importe</th>
    </tr>

  </thead>
  <tbody>
    @foreach ($pagos as $pago)
    <tr>
      <td>{{ $pago->codigopago }}</td>
      <td>{{ $pago->usuario }}</td>
      <td>{{ $pago->fecha }}</td>
      <td>{{ $pago->importe }}</td>
      <td><a href="{{url('/pagos/update')}}/{{$pago->codigopago}}">Editar</a> <a href="{{url('/pagos/delete')}}/{{$pago->codigopago}}">Borrar</a></td>
    </tr>

    @endforeach
  </tbody>
</table>
@endsection
