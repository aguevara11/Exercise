@extends('layouts.app')

@section('content')
<h1>Pagos</h1>
<form method="POST" action="{{url('/pagos')}}">
  <div class="form-group">
    <label>Usuario</label>
    <select class="form-control" name="codigousuario">
    	@foreach ($usuarios as $usuario)
      <option value="{{ $usuario->codigousuario }}">{{ $usuario->usuario }}</option >
    	@endforeach
    </select>
  </div>
  <div class="form-group">
    <label>Importe</label>
    <input class="form-control" type="number" name = "importe"></input>
  </div>
  <div class="form-group">
    <label>Fecha</label>
    <input class="form-control" type="text" name = "fecha" placeholder="2018-03-01"></input>
  </div>
{{csrf_field()}}
  <input type="submit" value="Create" class="btn btn-success">
  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
  @endif
</form>
@endsection
