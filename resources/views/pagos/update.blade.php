@extends('layouts.app')

@section('content')
<h1>Pagos</h1>
<form method="POST" action="{{url('/pagos/update')}}/{{$id}}">
  <div class="form-group">
    <label>Usuario</label>
    <select class="form-control" name="codigousuario">
    	@foreach ($usuarios as $usuario)
      <option value="{{ $usuario->codigousuario }}" {{($pagos[0]->codigousuario==$usuario->codigousuario )?'selected':''}}>{{ $usuario->usuario }}</option >
    	@endforeach
    </select>
  </div>
  <div class="form-group">
    <label>Importe</label>
    <input class="form-control" type="number" name = "importe" value="{{$pagos[0]->importe}}"></input>
  </div>
  <div class="form-group">
    <label>Fecha</label>
    <input class="form-control" type="text" name = "fecha" value="{{$pagos[0]->fecha}}"></input>
  </div>
{{csrf_field()}}
  <input type="submit" value="Update" class="btn btn-success">
  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
  @endif
</form>
@endsection
