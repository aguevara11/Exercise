@extends('layouts.app')

@section('content')
<a href="{{url('/favoritos/create')}}" role="button" class="btn btn-primary">Asociar Favorito</a>
<br>
<table class="table">
  <thead>
    <tr>
      <th scope="col">Usuario</th>
      <th scope="col">Favorito</th>
    </tr>

  </thead>
  <tbody>
    @foreach ($usuarios as $usuario)
    <tr>
      <td>{{ $usuario->usuario }}</td>
      <td>{{ $usuario->favorito }}</td>
      <td><a href="{{url('/favoritos/update')}}/{{$usuario->id_favorito}}">Editar</a> <a href="{{url('/favoritos/delete')}}/{{$usuario->id_favorito}}">Borrar</a></td>
    </tr>
    @endforeach
  </tbody>
</table>
@endsection
