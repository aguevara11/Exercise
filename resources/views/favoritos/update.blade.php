@extends('layouts.app')

@section('content')
<h1>Favoritos</h1>
<form method="POST" action="{{url('/favoritos/update')}}/{{$id}}">
	<div class="form-group">
		<select name="codigousuario" class="form-control">
			@foreach ($usuarios as $usuario)
		  <option value="{{ $usuario->codigousuario }}" {{($favoritos[0]->id_usuario==$usuario->codigousuario )?'selected':''}}>{{ $usuario->usuario }}</option >
			@endforeach
		</select>
	</div>
<div class="form-group">
	<select name="codigousuariofavorito" class="form-control">
		@foreach ($usuarios as $usuario)
	  <option value="{{ $usuario->codigousuario }}"  {{($favoritos[0]->id_favorito==$usuario->codigousuario )?'selected':''}}>{{ $usuario->usuario }}</option >
		@endforeach
	</select>
</div>
{{csrf_field()}}
  <input type="submit" value="Update" class="btn btn-success">
  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
  @endif
</form>
@endsection
