@extends('layouts.app')

@section('content')

<h1>Favoritos</h1>
<form method="POST" action="{{url('/favoritos')}}">
	<div class="form-group">
		<select name="codigousuario" class="form-control">
			@foreach ($usuarios as $usuario)
		  <option value="{{ $usuario->codigousuario }}">{{ $usuario->usuario }}</option >
			@endforeach
		</select>
	</div>
<div class="form-group">
	<select name="codigousuariofavorito" class="form-control">
		@foreach ($usuarios as $usuario)
	  <option value="{{ $usuario->codigousuario }}">{{ $usuario->usuario }}</option >
		@endforeach
	</select>
</div>
{{csrf_field()}}
  <input type="submit" value="Create" class="btn btn-success">
  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
  @endif
</form>
@endsection
