@extends('layouts.app')

@section('content')
<h1>Create</h1>
<form method="POST" action="{{url('/usuarios')}}">
  <div class="form-group">
    <label>Usuario</label>
    <input class="form-control" type="text" name="usuario" >
  </div>
  <div class="form-group">
    <label>Edad</label>
    <input class="form-control" type="number" name="edad" >
  </div>
  <div class="form-group">
    <label>Clave</label>
    <input class="form-control" type="password" name="clave" >
  </div>
  {{csrf_field()}}
  <input class="btn btn-success" type="submit" value="Create">
  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
  @endif
</form>
@endsection
