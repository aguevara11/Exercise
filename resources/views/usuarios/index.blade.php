@extends('layouts.app')

@section('content')
<a href="{{url('/usuarios/create')}}" role="button" class="btn btn-primary">Crear Usuario</a>
<br>
<table class="table">
  <thead>
    <tr>
      <th scope="col">Codigo Usuario</th>
      <th scope="col">Usuario</th>
      <th scope="col">Edad</th>
      <th scope="col">Acciones</th>
    </tr>

  </thead>
  <tbody>
    @foreach ($usuarios as $usuario)
    <tr>
      <td>{{ $usuario->codigousuario }}</td>
      <td>{{ $usuario->usuario }}</td>
      <td>{{ $usuario->edad }}</td>
      <td><a href="{{url('/usuarios/update')}}/{{$usuario->codigousuario}}">Editar</a> <a href="{{url('/usuarios/delete')}}/{{$usuario->codigousuario}}">Borrar</a></td>
    </tr>

    @endforeach
  </tbody>
</table>
@endsection
