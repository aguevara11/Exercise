<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Usuariospagos extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'codigousuario', 'codigopago',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

}
