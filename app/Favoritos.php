<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Favoritos extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'codigousuario', 'codigousuariofavorito', 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

}
