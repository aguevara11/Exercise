<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Pagos extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'importe', 'fecha',
    ];
    protected $primaryKey = 'codigopago';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

}
