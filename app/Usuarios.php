<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuarios extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'usuario', 'clave', 'edad',
    ];
    protected $primaryKey = 'codigousuario';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

}
