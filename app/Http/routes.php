<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/','HomeController@index');

Route::get('/usuarios','UsuariosController@index');
Route::post('/usuarios','UsuariosController@store');
Route::get('/usuarios/create','UsuariosController@create');
Route::get('/usuarios/update/{codigousuario}','UsuariosController@update');
Route::post('/usuarios/update/{codigousuario}','UsuariosController@update');
Route::get('/usuarios/delete/{codigousuario}','UsuariosController@delete');

Route::get('/favoritos','FavoritosController@index');
Route::post('/favoritos','FavoritosController@store');
Route::get('/favoritos/create','FavoritosController@create');
Route::get('/favoritos/update/{id}','FavoritosController@update');
Route::post('/favoritos/update/{id}','FavoritosController@update');
Route::get('/favoritos/delete/{id}','FavoritosController@delete');

Route::get('/pagos','PagosController@index');
Route::post('/pagos','PagosController@store');
Route::get('/pagos/create','PagosController@create');
Route::get('/pagos/update/{id}','PagosController@update');
Route::post('/pagos/update/{id}','PagosController@update');
Route::get('/pagos/delete/{codigopago}','PagosController@delete');
