<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Pagos;
use App\Usuariospagos;
use App\Usuarios;
use DB;
use Illuminate\Http\Request;

class PagosController extends Controller
{
  public function index(){
    $pagos = DB::select(DB::raw("SELECT u.codigousuario as codigousuario, p.codigopago, u.usuario, p.importe, p.fecha
                                    FROM pagos p
                                    join usuariospagos up on up.codigopago = p.codigopago
                                    join usuarios u on u.codigousuario = up.codigousuario"));

    return view('pagos.index',['pagos'=>$pagos]);
  }

  public function create(){
    $usuarios = Usuarios::all();
    return view('pagos.create',['usuarios'=>$usuarios]);
  }

  public function delete($codigopago){
    Pagos::destroy($codigopago);
    return redirect('/pagos');

  }

  public function update($id){
    if(!empty($_POST)){
    
       $pagos = Pagos::where('codigopago', $id)->first();
       $pagos->fecha = $_POST['fecha'];
       $pagos->importe = $_POST['importe'];
       $pagos->save();
       $usuariospagos = Usuariospagos::where('codigopago', $id)->get();
       
       foreach ($usuariospagos as $up) {
       	$usuariospagos->codigousuario = $_POST['codigousuario'];
       }  	
       
       return redirect('/pagos');
    }
    $usuarios = Usuarios::all();
    $pagos = DB::select(DB::raw("SELECT u.codigousuario as codigousuario, p.codigopago, u.usuario, p.importe, p.fecha
                                    FROM pagos p
                                    join usuariospagos up on up.codigopago = p.codigopago
                                    join usuarios u on u.codigousuario = up.codigousuario
                                    where p.codigopago = $id;
                                    "));
    return view('pagos.update',['usuarios'=>$usuarios,'pagos'=>$pagos,'id'=>$id]);
  }

  public function store(Request $request){

    $this->validate($request, [
          'codigousuario' => 'required',
          'fecha'   => 'required|date|after:today',
          'importe'    => 'required|integer|between:1,9999999999'
        ],[
          'codigousuario.required'=>'Debe seleccionar un usuario',
          'importe.required'=>'El campo importe es requerido',
          'importe.integer'=>'El campo importe debe ser númerico',
          'importe.between'=>'debe ser mayor a 0',
          'fecha.required'=>'El campo fecha es requerido',
          'fecha.date'=>'El campo fecha tiene que ser de tipo date',
          'fecha.after'=>'La fecha tiene que ser mayor a hoy',

        ]
      );
      $pagos = new Pagos();
      $pagos->importe = $_POST['importe'];
      $pagos->fecha =  $_POST['fecha'];
      $pagos->save();
      $usuariospagos = new Usuariospagos();
      $usuariospagos->codigousuario = $_POST['codigousuario'];
      $usuariospagos->codigopago = $pagos->codigopago;
      $usuariospagos->save();
      return redirect('/pagos');


  }
}
