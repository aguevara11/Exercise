<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Usuarios;
use Illuminate\Http\Request;

class UsuariosController extends Controller
{
  public function index(){
    $usuarios = Usuarios::all();
    return view('usuarios.index',['usuarios'=>$usuarios]);
  }

  public function create(){
    return view('usuarios.create');
  }
  public function update($codigousuario){
    
    if(!empty($_POST)){
       $usuario = Usuarios::where('codigousuario', $codigousuario)->first();
       $usuario->usuario = $_POST['usuario'];
       $usuario->edad = $_POST['edad'];
       $usuario->clave = md5($_POST['clave']);
       $usuario->save();
       return redirect('/usuarios');
    }
    $usuario = Usuarios::where('codigousuario', $codigousuario)->first();
    return view('usuarios.update',['usuario'=>$usuario]);
  }
  public function delete($codigousuario){
    Usuarios::destroy($codigousuario);
    return redirect('/usuarios');

  }
  public function store(Request $request){

    $this->validate($request, [
          'usuario' => 'required',
          'clave'   => 'required',
          'edad'    => 'required|integer|between:18,9999999999'
        ],[
          'usuario.required'=>'El nombre de usuario es requerido',
          'clave.required'=>'El campo clave es requerido',
          'edad.required'=>'El campo edad es requerido',
          'edad.integer'=>'El campo edad tiene que ser númerico',
          'edad.between'=>'La edad tiene que ser mayor a 18',
        ]
      );
      $usuario = new Usuarios();
      $usuario->usuario = $_POST['usuario'];
      $usuario->edad = $_POST['edad'];
      $usuario->clave = md5($_POST['clave']);
      $usuario->save();
      return redirect('/usuarios');
  }
}
