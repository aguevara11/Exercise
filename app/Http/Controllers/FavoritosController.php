<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Usuarios;
use App\Favoritos;
use Illuminate\Http\Request;
use DB;

class FavoritosController extends Controller
{
  public function index(){
    $usuarios = DB::select(DB::raw("SELECT f.id as id_favorito,u.codigousuario as id_usuario, u.usuario as usuario, uf.usuario as favorito
                FROM usuarios u
                join favoritos f on f.codigousuario = u.codigousuario
                join usuarios uf on uf.codigousuario = f.codigousuariofavorito"));

    return view('favoritos.index',['usuarios'=>$usuarios]);
  }

  public function create(){
    $usuarios = Usuarios::all();
    return view('favoritos.create',['usuarios'=>$usuarios]);
  }

  public function update($id){

    if(!empty($_POST)){
       $usuario = Favoritos::where('id', $id)->first();
       $usuario->codigousuario = $_POST['codigousuario'];
       $usuario->codigousuariofavorito = $_POST['codigousuariofavorito'];
       $usuario->save();
       return redirect('/favoritos');
    }

    $favoritos = DB::select(DB::raw("SELECT f.id as id, uf.codigousuario as id_favorito,u.codigousuario as id_usuario, u.usuario as usuario, uf.usuario as favorito
                FROM usuarios u
                join favoritos f on f.codigousuario = u.codigousuario
                join usuarios uf on uf.codigousuario = f.codigousuariofavorito
                where f.id =$id
                "));
    $usuarios = Usuarios::all();
    return view('favoritos.update',['usuarios'=>$usuarios,'favoritos'=>$favoritos,'id'=>$id]);
  }

  public function delete($id){
    Favoritos::destroy($id);
    return redirect('/favoritos');

  }

  public function store(Request $request){

    $this->validate($request, [
          'codigousuario'           => 'required',
          'codigousuariofavorito'   => 'required',
        ],[
          'codigousuario.required'=>'Debe seleccionar un usuario',
          'codigousuariofavorito.required'=>'Debe seleccionar un usuario',

        ]
      );
      $favorito = new Favoritos();
      $favorito->codigousuario = $_POST['codigousuario'];
      $favorito->codigousuariofavorito = $_POST['codigousuariofavorito'];
      $favorito->save();
      return redirect('/favoritos');


  }
}
